package by.lpe.cachequick.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

public class CacheImpl<K, V> implements Cache<K, V> {
    private final int timeToLive;
    private final ConcurrentHashMap<K, CacheValue<V>> map;

    private class CacheValue<L> {
        private L value;
        private long lastAccessTime;

        CacheValue(L value) {
            this.value = value;
            this.lastAccessTime = System.currentTimeMillis();
        }
    }

    private class CacheCleaner extends Thread {

        private int cleanerInterval;

        CacheCleaner(int cleanerInterval) {
            this.cleanerInterval = cleanerInterval * 1000;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(cleanerInterval);
                } catch (InterruptedException e) {
                }
                cleanup();
            }
        }
    }

    public CacheImpl(int timeToLive, int cleanerInterval) {
        this.timeToLive = timeToLive * 1000;
        this.map = new ConcurrentHashMap<>();

        if (cleanerInterval != -1)
            startCleaner(cleanerInterval);
    }

    private void startCleaner(int cleanerInterval) {
        CacheCleaner cleaner = new CacheCleaner(cleanerInterval);
        cleaner.setDaemon(true);
        cleaner.start();
    }

    @Override
    public V get(K key) {
        CacheValue<V> cacheValue = map.get(key);
        V value = null;
        if (cacheValue != null) {
            value = cacheValue.value;
            cacheValue.lastAccessTime = System.currentTimeMillis();
        }
        return value;
    }

    @Override
    public void put(K key, V value) {
        map.put(key, new CacheValue<>(value));
    }

    private void cleanup() {
        long now = System.currentTimeMillis();
        Predicate<Map.Entry<K, CacheValue<V>>> isTimeOut = e-> e.getValue().lastAccessTime + timeToLive < now;
        map.entrySet().stream()
                .filter(isTimeOut)
                .map(Map.Entry::getKey)
                .forEach(map::remove);
    }
}
