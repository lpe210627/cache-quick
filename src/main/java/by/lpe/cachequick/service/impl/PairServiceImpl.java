package by.lpe.cachequick.service.impl;

import by.lpe.cachequick.cache.Cache;
import by.lpe.cachequick.exception.PairNotFoundException;
import by.lpe.cachequick.model.KeyParam;
import by.lpe.cachequick.model.Pair;
import by.lpe.cachequick.service.PairService;
import org.springframework.beans.factory.annotation.Autowired;

public class PairServiceImpl implements PairService {

    @Autowired
    private Cache<String, String> cache;

    @Override
    public Pair<String, String> getByKey(String key) {
        String value = cache.get(key);

        if (value == null)
            throw new PairNotFoundException(key);

        return new Pair<>(key, value);
    }

    @Override
    public KeyParam<String> save(Pair<String, String> pair) {
        cache.put(pair.getKey(), pair.getValue());
        return new KeyParam<>(pair.getKey());
    }
}
