package by.lpe.cachequick.service;

import by.lpe.cachequick.model.KeyParam;
import by.lpe.cachequick.model.Pair;

public interface PairService {

    Pair<String, String> getByKey(String key);

    KeyParam<String> save(Pair<String, String> pair);
}
