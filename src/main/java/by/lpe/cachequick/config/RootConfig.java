package by.lpe.cachequick.config;

import by.lpe.cachequick.cache.Cache;
import by.lpe.cachequick.cache.CacheImpl;
import by.lpe.cachequick.service.PairService;
import by.lpe.cachequick.service.impl.PairServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RootConfig {

    @Bean
    public Cache<String, String> cache(){
        return new CacheImpl<>(10, 2);
    }
    @Bean
    public PairService pairService(){
        return new PairServiceImpl();
    }
}
