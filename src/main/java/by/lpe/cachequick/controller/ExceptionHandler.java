package by.lpe.cachequick.controller;

import by.lpe.cachequick.exception.PairNotFoundException;
import by.lpe.cachequick.model.KeyParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(PairNotFoundException.class)
    protected ResponseEntity<KeyParam<Object>> handlePairNotFoundException(PairNotFoundException e){
        return new ResponseEntity<>(new KeyParam<>(e.getKey()), HttpStatus.NOT_FOUND);
    }
}
