package by.lpe.cachequick.controller;

import by.lpe.cachequick.model.KeyParam;
import by.lpe.cachequick.model.Pair;
import by.lpe.cachequick.service.PairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class PairController {

    @Autowired
    private PairService pairService;

    @GetMapping("/{key}")
    public Pair<String, String> getByKey(@PathVariable String key){
        return pairService.getByKey(key);
    }

    @PostMapping("/")
    public KeyParam<String> save(@RequestBody @Valid Pair<String, String> pair){
        return pairService.save(pair);
    }
}
