package by.lpe.cachequick.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pair<K, V> {
    @NotNull
    private K key;
    @NotNull
    private V value;
}
