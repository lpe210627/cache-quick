package by.lpe.cachequick.exception;

import by.lpe.cachequick.model.KeyParam;

public class PairNotFoundException extends RuntimeException {
    private Object key;

    public PairNotFoundException(Object key) {
        this.key = key;
    }

    public Object getKey() {
        return key;
    }
}
